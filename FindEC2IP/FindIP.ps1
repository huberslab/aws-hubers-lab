#!/usr/bin/env pwsh

#################################################################################################
#                           Aspect Software
#             Source Control - Release Management Team (SCRM)
#                            M.Hubers 2019
# Repo:  xxx
# UseAt: xxx
#
#################################################################################################
Param (
    [string] $FindIP = '',
    [string] $Accounts = 'ALL',
    [string] $Regions = 'ALL'
)

## Get our current script full path and get the root of our script.
    $ScriptFile = $MyInvocation.MyCommand.Definition
    $ScriptPath = Split-Path -parent $ScriptFile
    $ScriptRootPath = & git -C $ScriptPath rev-parse --show-toplevel

### Main settings for this script.
    $AWSAccountList = "$ScriptRootPath/AccountList.csv"

## Make this script work on both Linux and Windows and use this variable to detect do action that only works on a giving OS
    if ($null -eq $IsCoreCLR)  {$IsCoreCLR = $false}

## Load modules
   if ($IsCoreCLR) {
		Import-Module AWSPowerShell.NetCore
	} else {
		Import-Module AWSPowerShell
   }

## The main part of this script
$global:NowUTC = (Get-Date).ToUniversalTime()

function Set-AWSProfile {
    Param( $AwsAccountId )

    $Profilename = "StartStop_$AwsAccountId"  # For this script we want profile as "StartStop_0123456789"
    Try  {
        Set-AWSCredentials -ProfileName $Profilename
        return $Profilename
    }
    Catch {
        Write-Host "`n--WARN--  Profile '$Profilename' is not setup in AWS ~/.aws/credentials file.  Skipping $AwsAccountId! (SCRM-ERR-CODE-025)`n"
        return "NA"
    }
}

##################################################################
### the core of the script
##################################################################
write-host "`n--- Running SCRM AWS Find IP script ---"

### Build our list of account(s) to deal with
$AccountsList = @()
if ($Accounts -match "a") {
    ### Load our accounts list we support.
    $tmpList = (Get-Content "$ScriptRootPath/$AWSAccountList") -notlike '^#' | ConvertFrom-Csv
    foreach ($account in $tmpList) {
        $AccountsList += $account.Account
    }
} else {
    ### That user account(s) list and make it into a array of hashes
    $AccountsList = $Accounts -split "_"
}

### Loop though all the accounts looking for instances that match
foreach ($account in $AccountsList) {
    ### Set the profile to account.
    $ProfileName = Set-AWSProfile $account
    if ($ProfileName -eq 'NA') {continue}

    write-host "Looking for IP $FindIP in account $account ..."
    foreach ($region in Get-AWSRegion) {
        ### Deal with ec2 instance
        $instances = (Get-EC2Instance -Region $region -ProfileName $ProfileName).RunningInstance
        foreach ($Instance in $instances) {
            $IPs = @()
            $InstId = $Instance.InstanceId
            $InstanceState = $Instance.State.Name
            $InstancePriIP = $Instance.PrivateIpAddress
            $InstancePubIP = $Instance.PublicIpAddress

            $TagName = $Instance.Tags | Where-Object { $_.key -eq 'Name' } | Select-Object -Expand Value
            if ($TagName -eq $null) {
                $TagName = 'Unlisted'
            }

            foreach($networkInterface in $instance.NetworkInterfaces) {
                $IPs += $networkInterface.PrivateIpAddresses.PrivateIpAddress
            }


            if ($InstancePriIP -eq $FindIP -or $InstancePubIP -eq $FindIP) {
                ### Found it
                write-host "  -FOUND IT-- $account - $region - $InstId - $InstancePriIP - $InstancePubIP - $TagName"
            } else {
                
                write-host "  $InstId - $region - $IPs - $InstancePubIP - $TagName"
            }
        }
    }
}
