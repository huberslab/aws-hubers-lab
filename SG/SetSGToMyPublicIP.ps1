#!/usr/bin/env pwsh

#################################################################################################
#                         Set AWS SG with current public IP
#
# Notes:
#   This find any Security Group SG that have the pass in tag name and value and then look
#   for any of the rules that match the rule tag pass into the script and update the IP
#   to the public IP of what this script runs as unless you pass in an Public IP to update to.
#
#   You can list as many profilename you like if you use '_' as your separator
#
#  Mark.Hubers 2019
#################################################################################################
Param (
	[alias("help","man","?")] [switch] $OptHelp = $false,    ### Display help.
	[string] $SecurityGroupTagName = 'UpdateDynamicIP',
	[string] $SecurityGroupTagValue = 'yes',
   [string] $RuleDescription = 'Hubers_IP',
   [string] $MyPubIP = (Invoke-WebRequest -uri "https://api.ipify.org/").Content,
   [string] $ProfileName = 'hubers-lab',
	[string] $TestMode = 'yes'
)

## Get our current script full path and get the root of our script.
	$global:ScriptFile = $MyInvocation.MyCommand.Definition
	$global:ScriptPath = Split-Path -parent $global:ScriptFile
	$global:ScriptRootPath = $global:ScriptFile -replace '(.*)/environment/.*','$1/environment'

## Make this script work on both Linux and Windows and use this variable to detect do action that only works on a giving OS
   if ($null -eq $IsCoreCLR)  {$IsCoreCLR = $false}

## The following are Aspect Created module for code sharing with others scripts.
   if ($IsCoreCLR) {
		Import-Module AWSPowerShell.NetCore
	} else {
		Import-Module AWSPowerShell
   }
   
## Test if our cred and profile works
   try {
      Set-AWSCredential -ProfileName $ProfileName
   } catch {
      write-host "`n--ERROR-- Unknown profile '$ProfileName' or bad accesskey.  Aborting.`n"
   }


## Deal with our public IP Address
   $MyPubIP = (Invoke-WebRequest -uri "https://api.ipify.org/").Content
   $myPubIPasCIDR = "$MyPubIP/32"
   
Function ConvertTo-EC2Filter {
   Param(
      [Parameter( ValueFromPipeline, ValueFromPipelineByPropertyName)]
      [HashTable] $Filter
   )

   Begin { $ec2Filter = @() }
   Process
   {
      $ec2Filter = Foreach ($key in $Filter.Keys) {
         @{
            name   = $key
            values = $Filter[$key]
         }
      }
   }
   End { $ec2Filter }
}

## Display info
   write-host "--- Looking for Security Group SG to update public IP ---------------------------------------"
   write-host "  SG with tag: $SecurityGroupTagName and value: $SecurityGroupTagValue"
   write-host "  My public CIDR: $myPubIPasCIDR"

foreach ($Region in Get-AWSRegion) {
   ## Make filter to find SG with tag name and value listed as para's and then get list of SG that match
      $ec2Filter = ConvertTo-EC2Filter @{"tag:$SecurityGroupTagName" = $SecurityGroupTagValue}
      
      $secGroupList = Get-EC2SecurityGroup -Filter $ec2Filter -ProfileName $ProfileName -Region $Region

   foreach ($secGroup in $secGroupList) {
      Write-Host "     -------------------------------------------------------"
      Write-Host "     Found $($secGroup.GroupId) with tag '$SecurityGroupTagName=$SecurityGroupTagValue'"
      Write-Host "        Looking for a SG inbound rule that have tag '$RuleDescription'..."

      ## Loop though the SG list of IpPermissions (rules) 
      foreach ($IpPermission in $secGroup.IpPermissions) {

         ## Look for a rule that have our matching description string
         foreach ($Ipv4Range in $IpPermission.Ipv4Ranges) {
            if ($Ipv4Range.Description -match $RuleDescription) {
               Write-Host "        Found rule '$($Ipv4Range.CidrIp)' with description matching tag."
               Write-Host "        See if this rule IP '$($Ipv4Range.CidrIp)' match our new public IP '$myPubIPasCIDR' "

               if ($Ipv4Range.CidrIp -eq $myPubIPasCIDR) {
                  Write-Host "           Existing IP match.  No need to Update this rule."
               } else {
                  Write-Host "           Existing IP do not match.  Updating the rule to IP '$myPubIPasCIDR."

                  ## Create new IpProtocol obj to be able to delete just one IP if there are more per port.
                  $RemoveIpPermission = New-Object Amazon.EC2.Model.IpPermission 
                  $RemoveIpPermission.FromPort = $IpPermission.FromPort
                  $RemoveIpPermission.ToPort = $IpPermission.ToPort
                  $RemoveIpPermission.IpProtocol = $IpPermission.IpProtocol
                  $newIpRange = New-Object Amazon.EC2.Model.IpRange
                  $newIpRange.CidrIp = $Ipv4Range.CidrIp
                  $RemoveIpPermission.Ipv4Ranges = $newIpRange

                  Revoke-EC2SecurityGroupIngress -GroupId $secGroup.GroupId -IpPermissions $RemoveIpPermission `
                        -ProfileName $ProfileName -Region $Region

                  ## Create new IpProtocol obj to add a new IP for the port.
                  $newIpPermission = New-Object Amazon.EC2.Model.IpPermission 
                  $newIpPermission.FromPort = $IpPermission.FromPort
                  $newIpPermission.ToPort = $IpPermission.ToPort
                  $newIpPermission.IpProtocol = $IpPermission.IpProtocol
                  $newIpRange = New-Object Amazon.EC2.Model.IpRange
                  $newIpRange.CidrIp = $myPubIPasCIDR
                  $newIpRange.Description = $RuleDescription
                  $newIpPermission.Ipv4Ranges = $newIpRange

                  Grant-EC2SecurityGroupIngress -GroupId $secGroup.GroupId -IpPermissions $newIpPermission `
                        -ProfileName $ProfileName -Region $Region

               }
            }
         }
      }
   }
} ## EndOfRegion Loop
