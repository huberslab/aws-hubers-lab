#!/usr/bin/env pwsh

#################################################################################################
#                            M.Hubers 2021
# Repo:  xxx
# UseAt: xxx
#
#################################################################################################
Param (
    [string] $profilename = 'rme',
    [string] $region = 'us-west-2'
)

## Get our current script full path and get the root of our script.
    $ScriptFile = $MyInvocation.MyCommand.Definition
    $ScriptPath = Split-Path -parent $ScriptFile
    $ScriptRootPath = & git -C $ScriptPath rev-parse --show-toplevel

## Make this script work on both Linux and Windows and use this variable to detect do action that only works on a giving OS
    if ($null -eq $IsCoreCLR)  {$IsCoreCLR = $false}

## Load modules
   if ($IsCoreCLR) {
		Import-Module AWSPowerShell.NetCore
	} else {
		Import-Module AWSPowerShell
   }

##################################################################
### the core of the script
##################################################################
write-host "`n--- Running cleanup AMIs ---"


$amiName = ‘<The users needs to mention their AMI ID>’
$totalSize = 0
$today = get-date

$AMIs = Get-EC2Image -region $Region -profilename rme -Owner self
foreach ($AMI in $AMIs) {
    ## Get number of ss and size
    $ss_count = $AMI.BlockDeviceMapping.Count
    $ss_size  = $AMI.BlockDeviceMapping.ebs | Measure-Object -Property volumeSize -Sum | Select-Object -expand Sum
    $totalSize += $ss_size 
    $ami_date = [datetime]::Parse($AMI.CreationDate)
    $age = New-TimeSpan -Start $ami_date -End $today
    $name = $AMI.Name

    Write-Host "AMI: '$name'   SS_Count=$ss_count    SS_Total_Size=$ss_size   days_old=$($age.days)"
}

Write-Host ("Total: $totalSize")
exit 0



$today = get-date
$x = New-TimeSpan -Start $startdate -End $today
"$($x.days) $("days have passed since") $($startdate)"


$count = $myImage[0].BlockDeviceMapping.Count 
$mySnaps = @()
for ($i=0; $i -lt $count; $i++) { 
    $snapId = $myImage[0].BlockDeviceMapping[$i].Ebs | foreach {$_.SnapshotId}
    $mySnaps += $snapId
}
Write-Host “Unregistering” $amiName
Unregister-EC2Image $amiName
foreach ($item in $mySnaps) {
    Write-Host ‘Removing’ $item
    Remove-EC2Snapshot $item
}



### Loop though all the accounts looking for instances that match
foreach ($account in $AccountsList) {
    ### Set the profile to account.
    $ProfileName = Set-AWSProfile $account
    if ($ProfileName -eq 'NA') {continue}

    write-host "Looking for IP $FindIP in account $account ..."
    foreach ($region in Get-AWSRegion) {
        ### Deal with ec2 instance
        $instances = (Get-EC2Instance -Region $region -ProfileName $ProfileName).RunningInstance
        foreach ($Instance in $instances) {
            $IPs = @()
            $InstId = $Instance.InstanceId
            $InstanceState = $Instance.State.Name
            $InstancePriIP = $Instance.PrivateIpAddress
            $InstancePubIP = $Instance.PublicIpAddress

            $TagName = $Instance.Tags | Where-Object { $_.key -eq 'Name' } | Select-Object -Expand Value
            if ($TagName -eq $null) {
                $TagName = 'Unlisted'
            }

            foreach($networkInterface in $instance.NetworkInterfaces) {
                $IPs += $networkInterface.PrivateIpAddresses.PrivateIpAddress
            }


            if ($InstancePriIP -eq $FindIP -or $InstancePubIP -eq $FindIP) {
                ### Found it
                write-host "  -FOUND IT-- $account - $region - $InstId - $InstancePriIP - $InstancePubIP - $TagName"
            } else {
                
                write-host "  $InstId - $region - $IPs - $InstancePubIP - $TagName"
            }
        }
    }
}
