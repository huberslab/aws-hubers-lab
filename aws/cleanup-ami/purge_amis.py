#! /usr/bin/env python3
'''
Python lambda func to run on an interval to purge AMIs matching certain specs
after X amount of days. This also removes the AMIs snapshots.

Refer to the lambda_handler function to see the event data that can be passed
into the func.

This ONLY runs in a single AWS account, and scans ALL regions for matching AMIs
'''
import sys
from operator import itemgetter
from datetime import datetime
from dateutil.parser import parse
import logging
import boto3
from botocore import exceptions


# default is 90
PURGE_AFTER_DAYS_OLD = 90
# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Client.describe_images
# MATCH_PARAMS is the filter value used by the ec2.describe_images call
DEFAULT_MATCH_PARAMS = [
    {'Name': 'tag:creator', 'Values': ['jenkins']},
    {'Name': 'tag:ami_security_compliance', 'Values':
     ['cis', 'fips']}
]

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    '''
    event :  dict   {'aws_profile': 'saml',
                     'purge_after': 90,
                     'filters': [{'Name': 'x',
                                  'Values':['list', 'of', 'values]}]
                     'test': True}

    If filters isn't defined, the DEFAULT_MATCH_PARAMS global var will be
    used for a lookup value.
    '''
    test = bool(event.get('test', True))
    logger.info(f"Test mode: {test}")
    s = sess(profile=event.get('aws_profile', None))
    client = s.client('ec2')
    regions = get_regions(client)
    purge_after = int(event.get('purge_after', PURGE_AFTER_DAYS_OLD))
    logger.info(f"Regions we'll scan: {regions}")
    logger.info(f"Purging AMIs older than {purge_after} days")
    purged_amis = {}

    # now lets start scanning for AMIs to purge in each region
    for region in regions:
        r_client = s.client('ec2', region_name=region)
        # lets get our sorted list of AMIs based on matching tags
        results = get_images(r_client, region,
                             event.get('filters', DEFAULT_MATCH_PARAMS))
        # now lets extract the AMI-ID, Name, and Date into a sep list
        age_list = []

        # r_imageids contains a set of AMI IDs that are currently used as the
        # source AMI for a running instance
        # https://trail.everbridge.com/browse/AEN-849
        r_imageids = get_instances(r_client, region)

        logger.info(f'Matching AMIs found in region: {len(results)}')
        for ami in results:
            days_old = get_age(ami.get('CreationDate'))
            # if AMI is older than our specified days old age, lets add to
            # the queue for purge
            if days_old > purge_after:
                # if the AMI ID is the source AMI for a running ec2 instance,
                # we will not mark the AMI as ready to be purged
                if ami.get('ImageId') in r_imageids:
                    logging.info(f"Found {ami.get('ImageId')} to be the source"
                                 " AMI for a running instance. Skipping.")
                    continue

                ami_dict = build_ami_dict(ami, days_old)
                logging.debug(f"{ami.get('ImageId')} is {days_old} days old; "
                              "marking for purge")
                age_list.append(ami_dict)
        # now we add our regions purgeable AMIs to a dict mapping
        # region:amiInfo
        if age_list:
            purged_amis[region] = age_list
        logger.info(f"AMIs to be purged: {len(age_list)}")

    # now lets pass our compiled AMI list to the func to remove the AMI,
    # then pass the block device mappings to delete_snapshot
    delete_ami(s, purged_amis, test)

    # lets count of purgable items
    pc = purge_count(purged_amis)
    if test:
        m = f"Would have purged {pc} AMIs"
        logger.info(m)
    else:
        m = f"Purged {pc} AMIs"
        logger.info(m)

    return {
        'statusCode': 200,
        'body': m
    }


def build_ami_dict(ami, age):
    '''
    We loop thru all regions to find our matching AMIs, and append this data
    to a list. This func builds a dict for each AMI, which is appended to
    the list.
    '''
    ami_dict = {}
    ami_dict['Name'] = ami.get('Name')
    ami_dict['ImageId'] = ami.get('ImageId')
    ami_dict['CreationDate'] = ami.get('CreationDate')
    ami_dict['BlockDeviceMappings'] = ami.get('BlockDeviceMappings')
    ami_dict['DaysOld'] = age
    return ami_dict


def purge_count(purge_list):
    '''
    Simple func to count the AMIs in the list
    '''
    c = 0
    for x in purge_list.values():
        for _ in x:
            c += 1
    return c


def get_age(time):
    '''
    func to determine age -- returns age in days (int)
    '''
    raw_date = parse(time)
    date_obj = raw_date.replace(tzinfo=None)
    diff = datetime.now() - date_obj
    return int(diff.days)


def sess(profile=None):
    '''
    func to create boto3 session -- returns boto session obj
    we assume we will default to the default region
    '''
    if profile:
        logger.info(f"Starting boto3 session with profile {profile}")
        return boto3.Session(profile_name=profile)
    logger.info("Starting boto3 session")
    return boto3.Session()


def get_regions(ec2_client):
    '''
    func to return the list of regions to iterate thru - returns list of
    regions
    '''
    r = ec2_client.describe_regions()
    regions = [region.get('RegionName') for region in r['Regions']]
    return regions


def get_images(ec2_client, region, filters):
    '''
    func to retrieve SORTED list of AMIs matching certain criteria

    returns list of AMIs sorted by CreationDate.. old first/new last
    '''
    logger.info(f"Scanning for AMIs to purge in {region}")
    orig_list = ec2_client.describe_images(Owners=['self'], Filters=filters)
    results = sorted(orig_list['Images'], key=itemgetter('CreationDate'))
    return results


def get_instances(ec2_client, region):
    '''
    Added in AEN-849

    This func returns a set which contains the source AMIs used to provision
    all running EC2 instances in the region
    '''
    logger.debug(f"Pulling instances in {region} to check source ImageID")
    results = ec2_client.describe_instances()
    ret = set()

    for inst_item in results['Reservations']:
        for inst in inst_item['Instances']:
            ret.add(inst.get('ImageId'))
    return ret


def delete_ami(s, ami_map, test):
    '''
    func to deregister AMI
    data struct we will be wokring with looks like this:
    'us-west-2': [   {   'CreationDate': '2019-07-11T17:30:15.000Z',
                         'DaysOld': 110,
                         'ImageId': 'ami-09bf1833d9583b1a6',
                         'Name': 'cis-base-ubuntu-16.04-20190711-171432'},
                     {   'CreationDate': '2019-07-12T15:09:39.000Z',
                         'DaysOld': 110,
                         'ImageId': 'ami-0ecc6c76b5a583abc',
                         'Name': 'fips-base-ubuntu-16.04-20190712-145821'}
                ]
    '''
    for region, amis in ami_map.items():
        r_client = s.client('ec2', region_name=region)
        for ami in amis:
            if not test:
                # test=False
                logger.info(f"Deregistering AMI || {ami.get('DaysOld')}"
                            f" days old || {ami.get('Name')} || "
                            f"{ami.get('ImageId')} || {region}")
                # CODE HERE TO ACTUALLY DEREGISTER AMI
                # if test=true we run dryrun
                try:
                    r_client.deregister_image(ImageId=ami.get('ImageId'))
                except exceptions.ClientError as e:
                    logger.error(f"Error calling deregister: {e}")
                    raise
            else:
                # test=True
                logger.info("TEST || Would have deregistered AMI || "
                            f"{ami.get('DaysOld')} days old || "
                            f"{ami.get('Name')} || "
                            f"{ami.get('ImageId')} || {region}")

            if ami.get('BlockDeviceMappings'):
                # if there is a BlockDeviceMappings list, and there
                # is, lets make sure we blow away the snapshots too
                delete_snapshot(r_client, ami.get('BlockDeviceMappings'),
                                test=test)
    return


def delete_snapshot(ec2_client, snapshots, test):
    '''
    func to remove a snapshots from a BlockDeviceMappings
    '''
    for snapshot in snapshots:
        snapid = snapshot['Ebs']['SnapshotId']
        if not test:
            logger.info(f"Removing snapshot {snapid}")
            # CODE HERE TO ACTUALLY BLOW AWAY THE SNAPSHOT
            # if test=True, we run DryRun
            try:
                ec2_client.delete_snapshot(SnapshotId=snapid)
            except exceptions.ClientError as e:
                logger.error(f"Error calling delete_snapshot: {e}")
                raise
        else:
            # test=True
            logger.info(f"TEST || Would have removed snapshot {snapid}")
    return


def main():
    event = {'aws_profile': 'saml',
             'test': True}
    return lambda_handler(event, context={})


if __name__ == '__main__':
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    logger.setLevel(logging.INFO)
    main()
