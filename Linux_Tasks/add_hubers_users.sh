#!/bin/bash

#
#  Add Hubers memebers as users with admin rights to sudo to linux OS.
#
#  Easy way to run this script on a new linux system is to run the following command:
#   curl -Lfk http://bb/add_scrm_users.sh?raw | sudo bash
#

adduser () {
	userFull=$1
	user=$2
	userId=$3
	sshkey=$4

	useradd -u $userId  -G scm  -c "$userFull"  $user

	[ -d /home/$user/.ssh ] || mkdir -p /home/$user/.ssh

	chmod 700 /home/$user/.ssh
	chown $user:$user /home/$user/.ssh
	echo $sshkey > /home/$user/.ssh/authorized_keys
	chmod 600 /home/$user/.ssh/authorized_keys
	chown $user:$user /home/$user/.ssh/authorized_keys

	sudoersd_file="/etc/sudoers.d/$user"
	[ -d $sudoersd_file ] || echo "$user  ALL=(ALL:ALL) NOPASSWD:ALL" > $sudoersd_file

	gpasswd -a $user docker > /dev/null 2>&1
}

groupadd -g 750 scm
groupadd -g 751 docker > /dev/null 2>&1

adduser 'Mark Hubers' 'mhubers' 10001 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCumsqxy9mbIryC8MLt7+FP4B9ZWj/pyuPK92EFA2+wDWcgWEtsfMPsITB7LBGY9x50gWIcjZPLpsWMDDzngq9eOtAyJZpwS9JncxP8K58d3D8d5s9lu1DnVeG65Flp4mAtEwvjFcERUU7AODq447Z/NZTXSkXD5L2oFAwzK1kg1d2BNw+7gl0M1Q6mgOe6NFg7JbPDBAWOMoPgQr7q3UubWDVfjurZgb693fKRh7Vr121XCCI9wzOiapnuRmFc6a9mKqZupWR0pLIsk4qQli1EvwKYeIwnuw7tm8e+et6e4WIQzAVC6Zq6CeTvZCfnbD2+yeTuEL9IXfVQnDLRwcuD LinuxUser_mhubers'
adduser 'User1 Test' 'tester1'  10010 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZvqJsuMpdii/MT+xJ9lAEttDCw8HXD4TNTLeH5uojqBvdWqFohG7BGNvuSV4qIOZ6ZmomrmTnGnC+ICGv44q0OJrwA/u3mbPUrRbKM4oosiKNnfVM/GtQ8k7u3So4d/zVbNy4j7n+wrx/9poSqu3YnZASLy0xnz2OgEYCyhlCbifmbSpDI99rZyAWmKeUGijxioFMxSvFQQqhNk8ZmEyDQZivU0plsn2b7EuQCNS7IEQwUtRZ8BudVtZtnANjfPAylcJGzqiwi33eHccRj6Wdhl0T7lYMCKZTVYtiCK+uhC0OWLB3vTtoiy5bJQx3Ef0i4BYPiiDmg5VPQwlPTwJ3 LinuxUser_tester1'
adduser 'User2 Test' 'tester2'  10011 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZrcK/fTWplcPF18xdeIr36WQRz5u2IfJV99Q0NS3nYXOIOqwAnIa3iUt7JVdis0VVLUGVRz0BLlUv/PinDTcVmCYvh+zrozwRfIrKiil+UyRX/7NjRnyjRbCCEUln6hgh7sThNnAeFEB+hLyZzT4biB6Tu0QXD7FPF9fqev/HNl0RidtbDceBArtewxv3JNg1u6k/Xv0zFku4GRa5m/BJWp0uUeaO+XbiGO4TDJIBFKSmIqev2QL5VsNng1ZpAHCOj0OY2p0VTHcdSCQZNAWi5anEe1Tt3GPYhj5D+Wl/gS0X3dl5/8eIBkvY3KKfBfYgfSmc/DWomlKlxlHkeNoD LinuxUser_tester2'
