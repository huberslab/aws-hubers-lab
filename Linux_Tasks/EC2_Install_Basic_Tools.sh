#############################################################################################################################
#
#  Version: 1.0 Jun-2021 Hubers: Create script
#  curl -Lfk https://bitbucket.org/huberslab/aws-hubers-lab/raw/master/Linux_Tasks/EC2_Install_Basic_Tools.sh | sudo bash
#############################################################################################################################

### Changeable Main Variables
   ScriptName='EC2_Install_Basic_Tools.sh'

### Autoset Variables
   StartTime=`date`

### Get get started!
   printf "\n----------------------------------------------------------------------------------------"
   printf "\n-  Starting $ScriptName at $StartTime"
   printf "\n----------------------------------------------------------------------------------------\n"


### See that this have run before and if so we exit out.
   if [ -f /mgh/RanScript_$ScriptName ]; then
      printf "\n*** Detected flag RanScript_$ScriptName!  This mean that the script is being re-run on the same instance (dirty)."
      printf "Aborting and someday may make this script to pick up from last run install (dirty)."
      exit 1
   fi


### Create /mgh folder for logs and notes
   sudo mkdir -p -- /mgh/logs
   sudo chmod 777 /mgh/logs
   core_logs='/mgh/logs'
   echo "This instance been setup by script $ScriptName." | sudo tee /mgh/SetupByScript___$ScriptName > /dev/null

### Get OS and version info
   printf "\nGet the OS distribution and the version info ...\n"
   OS_ID=`sed -ne 's/\(^\w*\).*/\1/p' /etc/system-release`
   case $OS_ID in
      "CentOS" )
         local_os_user_id='centos'
         local_os_user_home="/home/$local_os_user_id"
         ;;
      "Amazon" )
         local_os_user_id='ec2-user'
         local_os_user_home="/home/$local_os_user_id"
         ;;
      * )
         error_exit "   Unsupported OS distribution."
         ;;
   esac
   echo "   Linux distribution: $OS_ID"
   echo "   Default local user home: $local_os_user_home"


### Get some info about EC2 instance
   export AWS_INSTANCE_ID=`curl -Lfks http://169.254.169.254/latest/meta-data/instance-id`
   export AWS_INSTANCE_TYPE=`curl -Lfks http://169.254.169.254/latest/meta-data/instance-type`
   export AWS_INSTANCE_MAC=`curl -Lfks http://169.254.169.254/latest/meta-data/mac`
   export AWS_AMI_ID=`curl -Lfks http://169.254.169.254/latest/meta-data/ami-id`


### Install some common tools
   if true; then
      echo ' '
      echo "-------------------------------------------------------------------"
      echo "Installing some core tools like jq, wget, zip, etc. ..."
      echo "-------------------------------------------------------------------"
      sudo yum install -y wget bzip2 unzip nmap git iperf3 htop iotop bind-utils libxml2-devel jq >> $core_logs/yum_install.log
      #sudo yum groupinstall -y 'Development Tools' >> $core_logs/yum_install.log 

      while [ $(pgrep yum) ]; do sleep 1; done

      ### Tools that only exist or needed for that Linux distribution
      case $OS_ID in
         "CentOS" )
            sudo yum install -y epel-release     >> $logYumFile
            sodo yum install -y traceroute bmon vim >> $logYumFile
            ;;
         "Amazon" )
            # nothing listed yet
            ;;
      esac

      echo "      Done yum updates and install tools  $(date)"
      while [ $(pgrep yum) ]; do sleep 2; done
      echo "      Done watching for yum processes  $(date)"
   fi


### Add come commmon env or path.
   if true; then
      echo ' '
      echo "-------------------------------------------------------------------"
      echo "Add some main path and env if needed here ..."
      echo "-------------------------------------------------------------------"
      mkdir -p ~/bin
      chmod 755 ~/bin
      echo 'export PATH="~/bin:$PATH"' | sudo tee /etc/profile.d/core_env.sh > /dev/null
      export PATH="~/bin:$PATH"
   fi


### Update some TCP keep-alive settings to help connection stay alive
   echo ' '
   echo "-------------------------------------------------------------------"
   echo "Addd some TCP/IP settings for keep-alive ..."
   echo "-------------------------------------------------------------------"
   echo "net.ipv4.tcp_keepalive_time=600"  | sudo tee    /etc/sysctl.d/tcp_keepalive_settings.conf
   echo "net.ipv4.tcp_keepalive_intvl=60"  | sudo tee -a /etc/sysctl.d/tcp_keepalive_settings.conf
   echo "net.ipv4.tcp_keepalive_probes=20" | sudo tee -a /etc/sysctl.d/tcp_keepalive_settings.conf


### Install remnote subline edit
   if true; then
      echo ' '
      echo "-------------------------------------------------------------------"
      echo "Install remnote subline edit ..."
      echo "-------------------------------------------------------------------"
      sudo curl -sLk -o /usr/local/bin/rsub https://raw.github.com/aurora/rmate/master/rmate
      sudo chmod a+x /usr/local/bin/rsub; sudo ln /usr/local/bin/rsub /usr/bin/rsub
   fi


### Install Docker 
   if true; then
      echo ' '
      echo "-------------------------------------------------------------------"
      echo "Installing Docker ..."
      echo "-------------------------------------------------------------------"

      sudo groupadd docker > /dev/null 2>&1
      sudo usermod -a -G docker $local_os_user_id

      case $OS_ID in
         "Amazon" )
            sudo yum install -y docker
            ;;
         "CentOS" )
            echo "   INFO:  Centos 7 installs very outdated docker.  To use newer docker (still outdated) is use docker-latest."
            echo "          Or use Amazon Linux 2 for much newer docker engine."
            sudo yum install -y docker 
            sudo yum install -y docker-latest
      esac
      while [ $(pgrep yum) ]; do sleep 2; done

      echo "   Installing docker-machine ..."
      sudo curl -sLk 'https://github.com/docker/machine/releases/download/v0.16.0/docker-machine-Linux-x86_64' -o "/usr/local/bin/docker-machine"
      sudo chmod +x /usr/local/bin/docker-machine

      echo "   Installing docker-compose ..."
      sudo curl -sLk https://github.com/docker/compose/releases/download/1.29.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
      sudo chmod +x /usr/local/bin/docker-compose

      sudo systemctl enable docker; sudo systemctl start docker
   fi


### Set our timezone to US-East.
   echo ' '
   echo "-------------------------------------------------------------------"
   echo "Set timezone to America/New_York ..."
   echo "-------------------------------------------------------------------"
   timedatectl set-timezone America/New_York
   echo "Timezone set et to America/New_York"


### Load Python 3 and AWS tools for Python
   if true; then
      echo ' '
      echo "-------------------------------------------------------------------"
      echo "Installing Python 3.x ..."
      echo "-------------------------------------------------------------------"
      case $OS_ID in
         "Amazon" )
            printf "\nInstalling Python 3.x ...\n"
            sudo yum install -y python3
            pip3 install --user virtualenv
            ;;
         "CentOS" )
            printf "\nInstalling Python 3.4 and partly python 3.6 (Due to Centos7 not fully support 3.6) ...\n"
            ### Centos7 only install 'pip' from v3.4
            sudo yum install -y python34 python34-pip python34-tools python34-virtualenv >> $core_logs/python_install.log
            sudo yum install -y python36  >> $core_logs/python_install.log
            if [ ! -e "/usr/bin/python3" ]; then
               sudo ln -s /usr/bin/python3.6 /usr/bin/python3 >> $core_logs/python_install.log
            fi
            ;;
      esac
      while [ $(pgrep yum) ]; do sleep 1; done

      ### Add some common python packages like AWS
      printf "   Installing some Python packages like AWS API for Python ...\n"
      sudo pip3 install awsebcli boto3 > $core_logs/Pip_install.log
      sudo pip3 install docker >> $core_logs/Pip_install.log
      sudo pip3 install jinja2-cli >> $core_logs/Pip_install.log

      echo "which python3: $(which python3)"
      python3 --version
   fi



### Create a file stating that this script was runned.  So it not get re-runned
   sudo touch /mgh/RanScript_$ScriptName

### Echo some helpful info
   echo ' '
   echo "-------------------------------------------------------------------"
   echo " Done loading all tools!   Some key info:"
   echo "-------------------------------------------------------------------"
   echo "   -INFO- AWS_INSTANCE_ID=$AWS_INSTANCE_ID"
   echo "   -INFO- AWS_AMI_ID=$AWS_AMI_ID"

### Done with this bash so exit out
   EndTime=`date`
   echo -e "\n---- Done running $ScriptName at $EndTime \n\n"
